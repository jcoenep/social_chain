## Social Chain Task

![Social Chain](https://pbs.twimg.com/profile_images/875386565207392256/VLHAxSIJ_400x400.jpg)

### Overview

* `data` folder includes all the data as received.
* All visualisation are made with [echarts](http://john-coene.com/htmlwidgets/echarts).
* Mobile responsive

### How to

Simply download the `html` files and open them in your browser of choice.

### Task I

> Please could you create a simple report from this for the week 05JUN17-11JUN17, using the data from week 29MAY17-04JUN17 for comparison, including top line data such as growth, engagement rates, reach, post rate and anything else you can draw from the data. This report should be straight forward and easily consumable (as these would be distributed to content creators who aren't number led people!) 
This is more to see how you, yourself, would decide to communicate what you find to someone who isn't a statistician. Also the style of reporting is completely left to you - open playing field here.

Here I went for a simple dashboard that allows content creators to easily have a glance at the data and because I understood this very task to be more about presenting and consolidating reports than a full-fledged analysis. Moreover it was also ideal for me to also explore the data.

This dashboard is the following file: `Social Chain I.html`: Note the multiple pages in the menu bar.

### Task 2

> I think its crucial for us to start putting in internal benchmarks for spend nowadays, so would like you to come up with a reasonable daily organic reach average per post on videos for GameByte, so if a certain post goes above this then we spend on it rather than spending on each and every post - This daily organic reach info will be in column I and you would then have to combine the uploads on that day and divide by the figure in column I  

This document is the following file: `Social Chain II.html`.

### Additional tasks

> It would also be good to know how we are growing in each country to start making future projections for page growth to see when India for example overtakes the US and then we will be able to put measures we can put in place to stop/curb this! 

I don't go into time series analysis to predict when which country overtakes whichl; this would take a while. I show a motion chart that puts to view growth per country and their respective rates. India is growing fast indeed and has overtaken two countries in number of fans in four weeks but the rate of growth in the US remains nonetheless greater.

This document is the following file: `Social Chain III.html`.

----------------

Let me know if you encounter any issue:

* jcoenep@hotmail.com
* 0086 185 1128 7546