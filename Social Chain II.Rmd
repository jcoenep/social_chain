---
title: "Social Chain II"
author: "John Coene"
date: "July 27, 2017"
output:
  html_document:
    toc: true
    toc_float: true
    toc_depth: 3
    theme: sandstone
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning = FALSE, error = FALSE)

libs <- c("dplyr", "echarts", "flexdashboard")
lapply(libs, library, character.only = TRUE)

source("get_dl_data.R")
all <- get_dl_data()

theme <- "helianthus" # echarts theme

names(all$videos) <- gsub("Lifetime.", "", names(all$videos))
all$videos$id <- 1:nrow(all$videos)
```

## Task

> I think its crucial for us to start putting in internal benchmarks for spend nowadays, so would like you to come up with a reasonable daily organic reach average per post on videos for GameByte, so if a certain post goes above this then we spend on it rather than spending on each and every post.

## Exploration

This section explores **videos only**, focusing on the reach.

Context:

* Date Range: 01 April 2017 - 27 July 2017
* `r nrow(all$videos)` video posts
* 503 organic posts and `r nrow(all$videos) - 503` paid posts

### Reach per post

```{r}
all$videos %>% 
  echart(id, width = "100%") %>% 
  ebar(Post.Total.Reach, name = "Total Reach") %>% 
  etitle("Total Reach", "Reach by video post") %>% 
  etooltip() %>%
  etoolbox_magic(type = list("line", "bar")) %>% 
  etheme(theme)
```

### Organic vs Paid

```{r}
all$videos %>% 
  mutate(Paid.Video.Views = as.numeric(paste0(Paid.Video.Views))) %>% 
  filter(Paid.Video.Views > 0) %>% 
  echart(Post.ID, width = "100%") %>% 
  ebar(Paid.Video.Views, stack = "grp", name = "Paid views") %>% 
  ebar(Organic.Video.Views, stack = "grp", name = "Organic views") %>% 
  etooltip() %>% 
  elegend() %>% 
  etheme(theme)
```

### Organic vs Paid Normalized

```{r}
all$videos %>% 
  mutate(Paid.Video.Views = as.numeric(paste0(Paid.Video.Views)),
         Total.Video.Views = as.numeric(paste0(Total.Video.Views)),
         Organic.Video.Views = as.numeric(paste0(Organic.Video.Views))) %>% 
  mutate(Paid.Video.Views = Paid.Video.Views / Total.Video.Views,
         Organic.Video.Views = Organic.Video.Views / Total.Video.Views) %>% 
  mutate(Paid.Video.Views = round(Paid.Video.Views, 2) * 100,
         Organic.Video.Views = round(Organic.Video.Views, 2) * 100) -> mutated

mutated %>% 
  filter(Paid.Video.Views > 0) %>% 
  arrange(desc(Paid.Video.Views)) %>% 
  echart(Post.ID, width = "100%") %>% 
  ebar(Paid.Video.Views, stack = "grp", name = "Paid views") %>% 
  ebar(Organic.Video.Views, stack = "grp", name = "Organic views") %>% 
  etooltip() %>% 
  elegend() %>% 
  etitle("Normalized Views", "Paid vs Organic") %>% 
  etheme(theme)

paid <- mutated %>% 
  filter(Paid.Video.Views > 0) %>% 
  summarise(mean = mean(Total.Video.Views))

org <- mutated %>% 
  filter(Paid.Video.Views == 0) %>% 
  summarise(mean = mean(Total.Video.Views))
```

Average number of views for posts that have **not** been promoted (pure organic views) stands at `r org$mean[1]` while the average number of views for posts that **have been promoted** is `r paid$mean[1]` which is an `r round(paid$mean/org$mean)` fold increase.

However, I must admit not being a great fan of averages as the mean fails to communicate much information. 1) I think it is no surprise to anyone that paid posts have a much greater reach than organic ones, 2) paid posts do not perform uniformely; some post see 86% of views coming from promotion while others only see 5%. Hence one often being told not to look at the average without looking at the distribution. So Let's plot the distribution of views for promoted posts.

```{r}
all$videos %>% 
  mutate(Paid.Video.Views = as.numeric(paste0(Paid.Video.Views)),
         Total.Video.Views = as.numeric(paste0(Total.Video.Views)),
         Organic.Video.Views = as.numeric(paste0(Organic.Video.Views))) -> distri 

breaks <- seq(0, max(distri$Total.Video.Views, na.rm = TRUE), by = 500000)
cut <- cut(distri$Total.Video.Views, breaks, right=FALSE)
freq = table(cut)
dist <- as.data.frame(t(freq))

dist %>% 
  echart(cut, width = "100%") %>% 
  ebar(Freq) %>% 
  etitle("Views Distribution of Paid posts", "Bin size = 500'000 views") %>% 
  etooltip() %>% 
  etheme(theme)
```

```{r}
views <- as.numeric(paste(all$videos$Total.Video.Views))
views <- sort(views)
bottom <- views[1:463]
top <- views[464:length(views)]
```

The majority of paid posts are on the left of the distribution, they have a comparatively low number of views. This is the usual distribution for social media networks; and it implies interesting properties, namely the Pareto law: *20% of the posts account for more than 70% of the views*, of that top 20%, 67 videos have been promoted and 48 were purely organic (0 paid views). This means that there is certainly room for improvement.

```{r}
all$videos %>% 
  mutate(Total.Video.Views = as.numeric(paste(Total.Video.Views)),
         Paid.Video.Views = as.numeric(paste0(Paid.Video.Views))) %>% 
  arrange(desc(Total.Video.Views)) %>% 
  slice(1:115) %>% 
  mutate(promoted = ifelse(Paid.Video.Views > 0, "yes", "no")) %>% 
  count(promoted) -> promoted
```

**Disclaimer:** The task specifies to look at the average reach to make recommendations regarding which video should be promoted. So I will do this here but I am not sure this is ideal. I reckon it should have to do with the velocity, looking at how quickly a video picks up engagement. The latter should be more indicative of whether a promotion will be "successful", however I do not have access to that data now.

## Which video should be promoted?

Here is the approach I can take given the exploration above; look at the disparity of the average organic reach of the most successful promotions and purely organic videos then infer a threshold from that. Caveat, I use the median rather than the mean in this case; it is much more robust for log-normal distributions (shown before).

```{r}
all$videos %>% 
  mutate(Paid.Video.Views = as.numeric(paste0(Paid.Video.Views)),
         Total.Video.Views = as.numeric(paste0(Total.Video.Views)),
         Organic.Video.Views = as.numeric(paste0(Organic.Video.Views))) -> mutated

mutated %>% 
  filter(Paid.Video.Views > 0) -> paid 

paid <- quantile(paid$Organic.Video.Views, na.rm = T)

mutated %>% 
  filter(Paid.Video.Views == 0) -> org

org <- quantile(org$Organic.Video.Views, na.rm = T)

all$videos %>% 
  mutate(Total.Video.Views = as.numeric(paste(Total.Video.Views)),
         Paid.Video.Views = as.numeric(paste0(Paid.Video.Views))) %>% 
  arrange(desc(Total.Video.Views)) %>% 
  slice(1:115) %>% 
  mutate(promoted = ifelse(Paid.Video.Views > 0, "yes", "no")) %>% 
  filter(promoted == "yes") -> best_prom

best_prom_org <- quantile(as.numeric(paste0(best_prom$Organic.Video.Views)), na.rm = T)
```

* Median organic views for non-promoted posts `r prettyNum(unname(org[3]), big.mark = "'")`
* Median organic views for promoted posts `r prettyNum(unname(paid[3]), big.mark = "'")`
* Median organic views for best promoted posts `r prettyNum(unname(best_prom_org[3]), big.mark = "'")`

Therefore I reckon promoting posts who have organically reached between `r prettyNum(unname(paid[3]), big.mark = "'")` and `r prettyNum(unname(best_prom_org[3]), big.mark = "'")` would do provide best value for money.