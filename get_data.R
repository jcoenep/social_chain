# read all files sent by Joe
get_all_data <- function(){
  files <- list.files("data")
  data <- list()
  
  date_ranges <- c("05/06/2017-11/06/2017", 
                   "12/06/2017-18/06/2017", 
                   "19/06/2017-25/06/2017", 
                   "26/06/2017-02/07/2017", 
                   "29/05/2017-04/06/2017")
  
  # get name from file or sheet names
  get_name <- function(x){
    n <- gsub(".xlsx||[[:punct:]][[:space:]]", "", x) # rename
    gsub("[[:space:]]", "_", n) # rename
  }
  
  # read all files
  for(i in 1:length(files)){
    path <- paste0("data/", files[i])
    sheets <- readxl::excel_sheets(path)
    all_sheets <- list()
    for(j in 1:length(sheets)){
      all_sheets[[j]] <- readxl::read_xlsx(path, sheet = sheets[j], skip = 1)
      all_sheets[[j]]$date_range <- date_ranges[i]
    }
    
    names(all_sheets) <- get_name(sheets)
    
    data[[i]] <- all_sheets
  }
  
  names(data) <- get_name(files) # rename
  
  # Consolidate data - wide form
  sheets <- get_name(sheets)
  clean <- lapply(sheets, data.frame)
  
  for(i in 1:length(clean)){
    for(j in 1:length(data)){
      df <- data[[j]][[grep(paste0("^", sheets[i], "$"), names(data[[j]]))]]
      clean[[i]] <- plyr::rbind.fill(clean[[i]], df)
    }
  }
  
  names(clean) <- sheets
  
  return(clean)
}